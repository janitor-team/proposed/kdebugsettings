Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kdebugsettings
Upstream-Contact: kde-devel@kde.org
Source: https://invent.kde.org/utilities/kdebugsettings

Files: *
Copyright: 2015, Aaron Honeycutt <aaronhoneycutt@kubuntu.org>
           2015-2020, Montel Laurent <montel@kde.org>
License: LGPL-2+

Files: po/ca/*
       po/ca@valencia/*
       po/uk/*
Copyright: 2015-2020, This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV-translations

Files: src/org.kde.kdebugsettings.appdata.xml
Copyright: 2019, Jonathan Riddell <jr@jriddell.org>
License: CC0-1.0

Files: debian/*
Copyright: 2015 Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
License: GPL-2+

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along
 with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 --
 On Debian systems, the complete text of the CC0 Public Domain Dedication
 can be found in `/usr/share/common-licenses/CC0-1.0’.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 --
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2’.

License: LGPL-2+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This library is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.
 --
 On Debian systems, the full text of the GNU Library General Public License
 version 2 can be found in the file `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2.1+3+KDEeV-translations
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 --
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1’,
 likewise, the complete text of the GNU Lesser General Public License version
 3 can be found in `/usr/share/common-licenses/LGPL-3’.
